﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonCameraController : MonoBehaviour
{
    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float rotSpeed = 3.0f;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;

    float rotX = 0;
    float rotY = 0;
    bool cursorLocked = true;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        Debug.Log("Character camera loaded");
    }

    void Update()
    {
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            cursorLocked = !cursorLocked;
            if (cursorLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (cursorLocked)
        {
            rotX += Input.GetAxis("Mouse X") * rotSpeed;
            transform.localRotation = Quaternion.Euler(0f, rotX, 0f);
            rotY += Input.GetAxis("Mouse Y") * rotSpeed;
            transform.GetChild(0).localRotation = Quaternion.Euler(-rotY, 0f, 0f);
        }

        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
        controller.Move(moveDirection * Time.deltaTime);
    }
}